### Clean Code

One of the best names the Uncle Bob give to the developers is authors. We are authors.
And one thing of authors is that they have readers, so is our responsibility to
communicate clearly with our readers. This responsibility is higher when we enter
the agile and scrum environment where the main concept is the team, and collaboration.
<br>
But its not enough to write the code well, but it has to be kept clean over time.
We’ve all seen code rot and degrade as time passes. So we must take an active role in preventing this degradation.
So the first rule of clean code is:

> If we all checked-in our code a little cleaner than when we checked it out, the code simply could not rot. The cleanup doesn’t have to be something big.
> Change one variable name for the better.

### Part 1. Meaningful Names

> The name of a variable, function, or class, should answer all the big questions. It should tell you why it exists, what it does, and how it is used. If a name requires a comment, then the name does not reveal its intent.

1. Explicitly<br>
   As cited in the clean code book, the variables for example should not be clean as in
   simple to see:
   <br>
   `$a = 1`
   <br>
   but must be explicit as:
   <br>
   `$statusOfGame = STATUS_ONE`

2. Avoid disinformation<br>
   Avoid short names, and words which have more than one meaning:
   `hp, aix, and sco`
   Do not refer to a grouping of accounts as an `accountList` unless
   it’s actually a `List`. The word `list` means something
   specific to programmers.
   If the container holding the accounts is not actually a `List`,
   it may lead to false conclusions.
   So `accountGroup` or `bunchOfAccounts` or just plain `accounts` would be better.

3. Use searchable names<br>
   With the use of IDE the work of programming is transformed a lot in searching
   code already written, so to help in this process is better to think in advance
   and name our variables, but mostly our functions in a searchable way.

4. Class Names should be nouns and not verbs. Avoid words like Manager, Processor, Data, or Info in the name of a class.

5. Method names should be verbs.

### Part 2. Functions

First rule of functions is that they should be small
<br>
The second rule is that they should be smaller than that

1. Block and indention
   <br>
   Functions should have at most two levels of indentions

2. Functions should do **one** thing
   <br>
   Avoid that one functions does two things, for example like calulating the value and printing it.

3. One level of abstraction per function
   <br>
   Statements in a functions should be at the same level of abstraction

4. Stepdown rule
   <br>
   We want to read the code like a top-down narrative

5. Number of arguments
   <br>
   The ideal number of arguments for a function maximum of three, but less is better.
6. Prefer Exception instead of returning error codes
7. DRY principle (Dont repeat yourself)
8. Structured programing
   <br>
   Structured programing as specified by Dijkstra, says that every function, or even a block within a function should have a single entry point and a single exit. Following this rule a function should have a single `return`statement, no `break` or `continue` on `for` statements, and never a `goto`

### Part 3. Formatting

### Part 4. Testing
